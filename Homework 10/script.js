const enterInput = document.getElementById('enter-password');
const confirmInput = document.getElementById('confirm-password');
const inputWrapper = document.getElementById('input-wrapper');
const enterShowIcon = document.getElementById('enter-eye');
const enterHideIcon = document.getElementById('enter-eye-slash');
const confirmShowIcon = document.getElementById('confirm-eye');
const confirmHideIcon = document.getElementById('confirm-eye-slash');
const warning = document.createElement('p');
const showInput = () => {
    if (enterInput.getAttribute('type') === 'password') {
        enterInput.setAttribute('type', 'text');
        enterShowIcon.style.display = 'none';
        enterHideIcon.style.display = 'inline';
    } else {
        enterInput.setAttribute('type', 'password');
        enterShowIcon.style.display = 'inline';
        enterHideIcon.style.display = 'none';
    }
}
const hideInput = () => {
    if (confirmInput.getAttribute('type') === 'password') {
        confirmInput.setAttribute('type', 'text');
        confirmShowIcon.style.display = 'none';
        confirmHideIcon.style.display = 'inline';
    } else {
        confirmInput.setAttribute('type', 'password');
        confirmShowIcon.style.display = 'inline';
        confirmHideIcon.style.display = 'none';
    }
}
const comparePasswords = () => {
    warning.innerText = 'Нужно ввести одинаковые значения';
    warning.style.color = 'red';
    if (enterInput.value === confirmInput.value) {
        enterInput.value = '';
        confirmInput.value = '';
        if (inputWrapper.childNodes.length > 7) inputWrapper.removeChild(inputWrapper.lastChild);
        alert('You are welcome!');
    } else if (enterInput.value !== confirmInput.value) {
        if (inputWrapper.childNodes.length === 7) inputWrapper.append(warning);
    }
}

