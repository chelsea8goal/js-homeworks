let buttons = document.querySelectorAll('.btn');
document.addEventListener('keydown', (event) => {
    for (let item of buttons) {
        item.style.background = 'black';
        if (event.code === `Key${item.innerText}` || event.code === item.innerText) item.style.background = 'blue';
    }
})