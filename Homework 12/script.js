let images = document.querySelectorAll('.image-to-show');
let currentImage = 0;
let slideInterval = setInterval(nextSlide, 3000);
let buttonStop = document.getElementById('btn-stop');
let buttonContinue = document.getElementById('btn-continue');
buttonStop.style.borderWidth = '4px';
buttonContinue.style.borderWidth = '4px';
buttonStop.style.borderColor='black';
buttonContinue.style.borderColor='black';

function nextSlide() {
    images[currentImage].className = 'image-to-show';
    currentImage = (currentImage + 1) % images.length;
    images[currentImage].className = 'image-to-show showing';
}
buttonStop.onclick = function () {
    if (slideInterval) {
        clearInterval(slideInterval);
        slideInterval = null;
        buttonStop.style.borderColor = 'red';
        buttonContinue.style.borderColor = 'black';
    } else if (!slideInterval) {
        return false;
    }
}
buttonContinue.onclick = function () {
    if (slideInterval){
        return false;
    }
    else if (!slideInterval) {
        buttonContinue.style.borderColor = 'green';
        buttonStop.style.borderColor = 'black';
        slideInterval = setInterval(nextSlide, 3000);
    }
}



