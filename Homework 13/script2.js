let buttonChangeTheme = document.getElementById('btn-change-theme');
let color = 'white';

buttonChangeTheme.onclick = function () {
    if (color === 'white') {
        color = 'yellow';
        document.body.style.background = color;
        localStorage.setItem('theme', color);
    } else if (color === 'yellow') {
        color = 'white';
        document.body.style.background = color;
        localStorage.setItem('theme', color);
    }
}
window.onload = function () {
    document.body.style.background = localStorage.getItem('theme');
}