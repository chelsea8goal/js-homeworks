$("#menu").on("click","a", function (event) {
    event.preventDefault();
    let id  = $(this).attr('href');
    let top = $(id).offset().top;
    $('body,html').animate({scrollTop: top}, 1600);
});

$("#MostPopularPosts").click(function(){
    $("main div.posts_list").slideToggle(500);
    return false;
});

window.onscroll = function() {
    let scrolled = window.pageYOffset || document.documentElement.scrollTop;
    if (scrolled > 300) {
       $("#upbutton").css("display", "block")
    } else {
        $("#upbutton").css("display", "none")
    }
}

$('.scrollUp').click(function(){
    $("html, body").animate({ scrollTop: 0 }, 700);
    return false;
});