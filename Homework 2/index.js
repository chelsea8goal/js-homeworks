do {
	var num = +prompt('Enter your number');
} while (!Number.isInteger(num));
if (num >= 5) {
	for (let i = 0; i <= num; i++) {
		if (i % 5 === 0) {
			console.log(i);
		}
	}
} else {
	alert('Sorry, no numbers');
}
