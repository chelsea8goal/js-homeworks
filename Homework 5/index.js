const createNewUser = () => {
    let currentDate = new Date();
    let firstName = prompt("Enter your name");
    let lastName = prompt("Enter your last name");
    let selectBirthdayDate = prompt("Enter your birthday date (text in format dd.mm.yyyy)");
    selectBirthdayDate = `${selectBirthdayDate.substr(3, 2)}.${selectBirthdayDate.substr(0,2)}.${selectBirthdayDate.substr(6,4)}`
    let convertBirthdayDate = new Date(selectBirthdayDate);
    let user = {
        firstName: firstName,
        lastName: lastName,
        birthday: convertBirthdayDate,
        getLogin() {
            let firstCharOfName = user.firstName.substr(0, 1);
            return firstCharOfName.toLowerCase() + user.lastName.toLowerCase();
        },
        getAge() {
            let currentYear = currentDate.getFullYear();
            let birthdayYear = convertBirthdayDate.getFullYear();
            let currentMonth = currentDate.getMonth();
            let birthdayMonth = convertBirthdayDate.getMonth();
            birthdayMonth++;
            let currentDay = currentDate.getDate();
            let birthdayDay = convertBirthdayDate.getDate();
            let age = currentYear - birthdayYear;
            if ( currentMonth < (birthdayMonth - 1))
            {
                age--;
            }
            if (((birthdayMonth - 1) === currentMonth) && (currentDay < birthdayDay))
            {
                age--;
            }
            return age;
        },
        getPassword() {
            let firstCharOfName = user.firstName.substr(0, 1);
            return firstCharOfName.toUpperCase() + user.lastName.toLowerCase() + convertBirthdayDate.getFullYear();
        }
    }

    return ` Your login: ${user.getLogin()}
    Your password: ${user.getPassword()}
    Your birthday: ${user.getAge()}`;
}
console.log(createNewUser());

