let arr = ['hello', 'world', 23, '23', null]

const filterBy = (arr, type) => arr.filter(function (item) {
    return typeof item !== type
});

console.log(filterBy(arr, "object"))