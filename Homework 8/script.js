const label = document.createElement('label')
const input = document.createElement('input');
const backgroundInput = document.createElement('div');
label.innerText = 'Price: '

label.append(input);
backgroundInput.append(label);
document.body.append(backgroundInput)

let closeButton;
let price;
let div;
let error;

const focusButton = () => {
    input.style.outline = "3px solid green";
    backgroundInput.style.background = '';

    if (error) {
        error.remove();
    }
}

const buttonBorder = () => {
    input.style.outline = "";
    if(input.value > 0) {
        div = document.createElement('div');
        price = document.createElement('span');
        closeButton = document.createElement('button');

        closeButton.innerText = 'X';
        price.innerText = `Current price: ${input.value}$.`;

        div.append(closeButton);
        div.prepend(price);
        document.body.prepend(div);

        backgroundInput.style.backgroundColor = 'green'

        closeButton.addEventListener('click', event => {
            if (event.target.tagName === 'BUTTON') {
                event.target.parentNode.remove();
                input.value = '';
                backgroundInput.style.backgroundColor = 'red';
            }
        });

    } else if (input.value <= 0) {
        input.style.outline = "3px solid red";
        error = document.createElement('p');
        error.innerText = 'Please enter correct price';
        document.body.append(error);
    }
   input.value = '';
}
input.addEventListener('focus', focusButton);
input.addEventListener('blur', buttonBorder);
